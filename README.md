# Boolean Modeling of Parkinson's Disease

This repository contains Python and R scripts for analyzing and modeling Parkinson's disease mechanisms using Boolean modeling and data processing tools. The project uses MaBoSS simulations and additional analytical pipelines to extract insights into Parkinson's disease pathways.

---

## Table of Contents
- [Overview](#overview)
- [Setup Instructions](#setup-instructions)
  - [Python Environment](#python-environment)
  - [R Environment](#r-environment)
- [Scripts and Usage](#scripts-and-usage)
  - [Python Script](#python-script)
  - [R Scripts](#r-scripts)
- [Dependencies](#dependencies)
- [Notes](#notes)
- [License](#license)
- [Contact](#contact)

---

## Overview

This project focuses on Boolean modeling for Parkinson's disease to identify key states and pathways in disease progression. It uses MaBoSS simulations for probabilistic Boolean network modeling and integrates data analysis pipelines written in R and Python.

The repository is hosted at:  
[Boolean Modeling of Parkinson's Disease GitLab Repository](https://gitlab.lcsb.uni.lu/lcsb-biocore/publications/hemedan23-boolean-modelling-of-pd/-/tree/main/Results?ref_type=heads)

---

## Setup Instructions

### Python Environment

1. **Clone the Repository**
   ```bash
   git clone https://gitlab.lcsb.uni.lu/lcsb-biocore/publications/hemedan23-boolean-modelling-of-pd.git
   cd publications/hemedan23-boolean-modelling-of-pd
   ```

2. **Create and Activate a Virtual Environment**
   - On macOS/Linux:
     ```bash
     python3 -m venv .venv
     source .venv/bin/activate
     ```
   - On Windows:
     ```bash
     python -m venv .venv
     .venv\Scripts\activate
     ```

3. **Install Dependencies**
   Install the required Python libraries:
   ```bash
   pip install -r requirements.txt
   ```

---

### R Environment

1. **Install `renv`**
   Make sure you have `renv` installed in R. If not, install it:
   ```R
   install.packages("renv")
   ```

2. **Restore R Environment**
   Run the following in the R console to restore the package environment:
   ```R
   renv::restore()
   ```

---

## Scripts and Usage

### Python Script

**Script:** `MBSS_FormatTable.py`  
This script processes MaBoSS simulation outputs to generate probability tables and state distributions.

#### **Dependencies**
- `maboss`
- `pandas`

#### **Usage**
Run the Python script as follows:
```bash
python MBSS_FormatTable.py <file.bnd> <file.cfg> [<optional_threshold>] [-mb <maboss_executable>]
```

**Parameters:**
- `<file.bnd>`: Boundary condition file.
- `<file.cfg>`: Configuration file.
- `<optional_threshold>`: Probability threshold (optional).
- `-mb <maboss_executable>`: Path to the MaBoSS executable (optional).

**Example:**
```bash
python MBSS_FormatTable.py example.bnd example.cfg 0.01 -mb /path/to/maboss
```

---

### R Scripts

#### **1. `parsingTraj.R`**
This script extracts probability trajectories from MaBoSS `.probtraj` files.

**Dependencies**
- `optparse`
- `data.table`

**Usage**
Run the script with required arguments:
```bash
Rscript parsingTraj.R -i <input_file> -o <output_prefix> -n "<conditions>" [options]
```

**Parameters:**
- `-i`: Input `.probtraj` file.
- `-o`: Output file prefix (default: `StateProbabilities.txt`).
- `-n`: Conditions to extract trajectories for.
- `-s`: States for each condition (optional).

**Example:**
```bash
Rscript parsingTraj.R -i input.probtraj -o output -n "phenotype1,phenotype2"
```

---

#### **2. `workflowRun.R`**
This script implements workflows for Boolean modeling using provided `.bnd` and `.cfg` files.

**Dependencies**
- `BoolNet`
- `clusterProfiler`
- `pathview`

**Usage**
Run the script as follows:
```bash
Rscript workflowRun.R -b <boundary_file> -c <config_file> -p <parameter_file>
```

**Parameters:**
- `-b`: Boundary file path.
- `-c`: Configuration file path.
- `-p`: Parameter file path.

**Example:**
```bash
Rscript workflowRun.R -b model.bnd -c config.cfg -p parameters.csv
```

---

## Dependencies

### Python Dependencies
- `maboss`
- `pandas`

Install them with:
```bash
pip install -r requirements.txt
```

### R Dependencies
The `renv` package is used for managing R dependencies. Run:
```R
renv::restore()
```

---

## Notes

- Ensure you have the required MaBoSS binaries installed and accessible if using the Python script.
- Use `.gitignore` to avoid committing large or unnecessary files like the `.venv` directory or intermediate results.

---

## License

This project is distributed under the [MIT License](LICENSE).

---

## Contact

For questions or issues, please contact:  
[Ahmed Hemedan](ahmed.hemedan@uni.lu)

Alternatively, open an issue in the [GitLab Repository](https://gitlab.lcsb.uni.lu/lcsb-biocore/publications/hemedan23-boolean-modelling-of-pd/-/issues).

