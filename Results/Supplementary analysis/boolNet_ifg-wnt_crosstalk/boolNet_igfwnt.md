---
Title: "A Boolean network of the crosstalk between IGF and Wnt signaling"
By: "(Ahmed Abdelmonem Hemedan)"
date: "3/18/2020"
---
# A Boolean network of the crosstalk between IGF and Wnt signaling
# Aim
This work aimed to create a Boolean network model, which accurately describes the crosstalk between IGF and Wnt signalling as it was observed in satellite cells. The knowledge incorporated into this model originates from extensive literature research. The model was then used to simulate the interaction of both IGF and Wnt signalling in the context of ageing.
# Method
* Synchronous updating scheme using extensive algorithm in BoolNet to search all attractors in all paths by computing forward and backward reachable sets of seed.
* Implement four layers of asynchronous scheme and compare -> to do
# Result
The simulation returned five attractors that represent temporal state phenotypes. Attractors confirmed the crosstalks and shifting behaviour from IGF to Wnt
1.	**Signaling cascade of an un-stimulated cell**
* GSK3b is active because it is ubiquitously expressed and stays active if not otherwise inhibited.
 * Downstream targets of GSK3b are S6K and TSC2 which both can be activated by GSK3β resulting in an active state in this attractor.   

2.	**This attractor represents a young phenotype**
* IGF initiates the Ras-Raf-MAPK-cascade leading to ERK activation.
* Though PI3K-Akt cascade is initially inactivated due to a negative feedback, it is partially active in the attractor.
* Influence of the crosstalk on Wnt -> the activation b_catenin and TCF as well as Rho, Rac, PKC and JNK.
* IGF and Ras as well as the canonical Wnt antagonist axin 2 are the only nodes that are active in all three states. All other nodes switch between their active and inactive state during iterations.

3.	**Signaling cascade of the of IGF and Wnt—A mid-aged phenotype**
* During aging, IGF signaling declines whereas Wnt signaling increases slowly and therefore the resulting a mid-aged phenotype.
* IGF and Wnt are active  the model reaches a single state attractor.
*  β-catenin is activated. However, its downstream target TCF is not active in the entire signaling cascade.
*  TCF competes with FoxO for the binding to β-catenin.
*  Due to the activation of Akt by IGF, FoxO gets inactivated and thus TCF is again active in the attractor.

4.	**Signaling cascade of Wnt—An aged-phenotype**
 * β-catenin is activated. Despite the inactive IGF, TCF is not active over the entire signaling cascade. Here, * it competes again with FoxO until it is inactivated by Akt due to crosstalk.
* In addition, Ras-Raf-MAPK signaling is also activated by crosstalk, even if IGF is not present.
* This crosstalk is mediated by factors of non-canonical Wnt signaling such as PKC, Rac and JNK.
* The resulting attractor is a single state attractor with active canonical and non-canonical Wnt signaling as well as components of IGF signaling.


# Discussion
* IGF and Wnt molecules can affect cells in a paracrine manner. Not necessarily being secreted by the Wnt or IGF affected cells themselves, they cannot be activated within the network. Therefore, in this model, IGF and Wnt are considered inputs.Canonical Wnt signalling is activated by Wnt binding to a Frizzled receptor (Fzd) and the co-receptor Lipoprotein receptor-related protein.
* This receptor complex, in turn, activates Dishevelled (Dvl). In the non-canonical Wnt/JNK pathway, Dvl is activated similarly, although a different co-receptor, the Receptor tyrosine kinase-like orphan receptor (Ror), is used. Wnts can also activate the small GTPases Rac and Rho. Rac is responsible for cell polarization. Rac can further activate Mitogen-activated protein kinase kinase kinase 1 (MEKK1).
* To activate Wnt/calcium signalling a Wnt ligand activates a G-protein coupled to a Frizzled receptor and Dvl to stimulate Phospholipase C (PLC), the latter of which hydrolyses Phosphatidylinositol-(4,5)-bisphosphate into diacylglycerol (DAG) and Inositiol-(1,4,5)-trisphosphate. This facilitates calcium influx and in turn, DAG can further activate Protein kinase C (PKC). 
* Additionally, PKC can be activated downstream of Rho. PKC can inhibit Rho, thereby creating a negative feedback loop. β-catenin, the key effector of canonical Wnt signalling, can be phosphorylated and primed for proteasomal degradation by a destruction complex (DC) including the glycogen synthase kinase 3 beta (GSK3β) and axin 2, which triggers β-catenin inactivation. Dvl (here via Wnt) inhibits GSK3β and consequently, β-catenin is stabilized and can translocate into the nucleus where it activates the transcription factors of the T-cell factor family (TCF) or FoxO. In the next step Ras, which can also be activated by Dvl (here via Wnt), activates Raf and this then activates ERK.
* IRS activates PI3K and this further activates Akt. Another activator of PI3K is Ras. PI3K activates Rho and Rac. A downstream target of Rho termed Rho-associated protein kinase (ROCK) activates phosphatase and tensin homolog (PTEN), which is an inhibitor of PI3K. As a consequence, a negative feedback loop is created.

* Downstream of PI3K Akt inhibits Tuberous Sclerosis Factor 2 (TSC2) by phosphorylation thereby activating mTORC1, which is otherwise inhibited by TSC2. ERK can inhibit TSC2 as well. The activated mTORC1 further activates p70-S6 kinase (S6K). To avoid excessive IGF signalling a feedback inhibition of IRS by S6K is initiated. Besides, S6K is an inhibitor of mTORC2. Association of the complex with ribosomes through PI3K seems to be necessary as well as active TSC2. GSK3β can p activate TSC2. Also, GSK3β activates S6K. GSK3β can also be inhibited by ERK or Akt.
* Likewise, Raf can be inhibited by Akt. Another cross-regulation is the activation of axin 2 by ERK. Besides the activation through PI3K, Akt can be activated by mTORC2 through phosphorylation. A very common interaction of the Wnt/calcium pathway and IGF signalling is the activation of Raf by PKC while Raf activation through canonical Wnt signalling is assumed. 
* As part of non-canonical Wnt signalling, Rac can activate JNK. Another way of activating JNK is mediated by MEKK1. By inhibiting IRS JNK can reduce IGF signalling.

* Both FoxO and JNK are part of the cellular stress response and  JNK is a strong activator of FoxO. TCF requires β-catenin to function as a transcription activator. However, FoxO is also able to bind β-catenin and act as a transcription factor. Thus, they have to compete for binding of β-catenin and their activation is mutually exclusive. Consequently, they inhibit each other. JNK is a potent activator of FoxO and thus contributes to the inhibition of TCF via FoxO. FoxO is a transcription factor for Rictor, which is a component of mTORC2.
* The exhaustive search algorithm to identify all the attractors of the model synchronously. Five attractors named attractor 1 to attractor 5 were identified with a different frequency.
* Four of these attractors were single-state attractors and one was a cyclic attractor with three states. Depending on the initial state the nodes representing the Wnt and IGF inputs stay active or inactive over the entire period.Thus, there are four possible combinations of external input factors for this model.
* Both IGF and Wnt are active (attractor 3). Only IGF or only Wnt is active (attractors 2 and 5, respectively attractor 4).Both are inactive (attractor 1). Attractor 1 represents the state of a un-stimulated cell neither IGF nor Wnt are active and thus no input is given. During ageing, IGF signalling slowly declines while both canonical and non-canonical Wnt signalling increases. Based on this it is assumed stimulation by IGF only results in a young phenotype (attractors 2 and 5) and stimulation by Wnt an aged phenotype (attractor 4).
* However, when starting a simulation from an initial state where all nodes except the input factors are off, every attractor could be reached, except for attractor 5. Since attractor 5 could not be reached starting from the chosen initial state it was excluded from further analysis.Attractors 3 and 4 represent the age-related shift from IGF to Wnt signalling. Due to the slow transformation process first, both IGF and Wnt are active (attractor 3) and afterwards only Wnt as external input is active (attractor 4).
