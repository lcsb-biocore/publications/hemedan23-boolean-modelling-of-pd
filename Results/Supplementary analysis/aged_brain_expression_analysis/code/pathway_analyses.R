
# Pathway analyses to understand the further perturbation and simulation experiment we will do



# Load pathway definitions from MSigDB:

msigdb_go_pathways = read.gmt("c5.all.v6.2.symbols.gmt")
msigdb_kegg_pathways = read.gmt("c2.cp.kegg.v6.2.symbols.gmt")
msigdb_reactome_pathways = read.gmt("c2.cp.reactome.v6.2.symbols.gmt")
msigdb_biocarta_pathways = read.gmt("c2.cp.biocarta.v6.2.symbols.gmt")
msigdb_positional = read.gmt("c1.all.v6.2.symbols.gmt")

# Inspect top of the pathway annotation table for GO:
head(msigdb_go_pathways)


# Apply classical Fisher's Exact test (significance-of-overlap computation) - Zhang

fisher_go_zhang <- enricher(zhang_degs, universe = mapped_symbols, pAdjustMethod = "BH", pvalueCutoff=1.0, qvalueCutoff = 0.2, TERM2GENE = msigdb_go_pathways)
head(fisher_go_zhang)

fisher_kegg_zhang <- enricher(zhang_degs, universe = mapped_symbols, pAdjustMethod = "BH", pvalueCutoff=1.0, qvalueCutoff = 0.2, TERM2GENE = msigdb_kegg_pathways)
head(fisher_kegg_zhang)

fisher_biocarta_zhang <- enricher(zhang_degs, universe = mapped_symbols, pAdjustMethod = "BH", pvalueCutoff=1.0, qvalueCutoff = 0.2, TERM2GENE = msigdb_biocarta_pathways)
head(fisher_biocarta_zhang)

fisher_reactome_zhang <- enricher(zhang_degs, universe = mapped_symbols, pAdjustMethod = "BH", pvalueCutoff=1.0, qvalueCutoff = 0.2, TERM2GENE = msigdb_reactome_pathways)
head(fisher_reactome_zhang)

fisher_positional_zhang <- enricher(zhang_degs, universe = mapped_symbols, pAdjustMethod = "BH", pvalueCutoff=1.0, qvalueCutoff = 0.2, TERM2GENE = msigdb_positional)
head(fisher_positional_zhang)


# Apply classical Fisher's Exact test (significance-of-overlap computation) - Moran 

fisher_go_moran <- enricher(moran_degs, universe = mapped_symbols, pAdjustMethod = "BH", pvalueCutoff=1.0, qvalueCutoff = 0.2, TERM2GENE = msigdb_go_pathways)
head(fisher_go_moran)

fisher_kegg_moran <- enricher(moran_degs, universe = mapped_symbols, pAdjustMethod = "BH", pvalueCutoff=1.0, qvalueCutoff = 0.2, TERM2GENE = msigdb_kegg_pathways)
head(fisher_kegg_moran)

fisher_biocarta_moran <- enricher(moran_degs, universe = mapped_symbols, pAdjustMethod = "BH", pvalueCutoff=1.0, qvalueCutoff = 0.2, TERM2GENE = msigdb_biocarta_pathways)
head(fisher_biocarta_moran)

fisher_reactome_moran <- enricher(moran_degs, universe = mapped_symbols, pAdjustMethod = "BH", pvalueCutoff=1.0, qvalueCutoff = 0.2, TERM2GENE = msigdb_reactome_pathways)
head(fisher_reactome_moran)

fisher_positional_moran <- enricher(moran_degs, universe = mapped_symbols, pAdjustMethod = "BH", pvalueCutoff=1.0, qvalueCutoff = 0.2, TERM2GENE = msigdb_positional)
head(fisher_positional_moran)


# Apply GSEA - Zhang 

ranked_genelst = ttable_zhang$B
names(ranked_genelst) = rownames(ttable_zhang)

gsea_go_zhang = GSEA(ranked_genelst, exponent = 1, nPerm = 1000, minGSSize = 10,
                     maxGSSize = 500, pvalueCutoff = 1, pAdjustMethod = "BH", TERM2GENE = msigdb_go_pathways,
                     TERM2NAME = NA, verbose = TRUE, seed = FALSE, by = "fgsea")
head(gsea_go_zhang)

gsea_kegg_zhang = GSEA(ranked_genelst, exponent = 1, nPerm = 1000, minGSSize = 10,
                       maxGSSize = 500, pvalueCutoff = 1, pAdjustMethod = "BH", TERM2GENE = msigdb_kegg_pathways,
                       TERM2NAME = NA, verbose = TRUE, seed = FALSE, by = "fgsea")
head(gsea_kegg_zhang)

gsea_reactome_zhang = GSEA(ranked_genelst, exponent = 1, nPerm = 1000, minGSSize = 10,
                           maxGSSize = 500, pvalueCutoff = 1, pAdjustMethod = "BH", TERM2GENE = msigdb_reactome_pathways,
                           TERM2NAME = NA, verbose = TRUE, seed = FALSE, by = "fgsea")
head(gsea_reactome_zhang)

gsea_positional_zhang = GSEA(ranked_genelst, exponent = 1, nPerm = 1000, minGSSize = 10,
                             maxGSSize = 500, pvalueCutoff = 1, pAdjustMethod = "BH", TERM2GENE = msigdb_positional,
                             TERM2NAME = NA, verbose = TRUE, seed = FALSE, by = "fgsea")
head(gsea_positional_zhang)
gsea_positional_zhang

# Apply GSEA - Moran 


ranked_genelst = ttable_moran$B
names(ranked_genelst) = rownames(ttable_moran)

gsea_go_moran = GSEA(ranked_genelst, exponent = 1, nPerm = 1000, minGSSize = 10,
                     maxGSSize = 500, pvalueCutoff = 1, pAdjustMethod = "BH", TERM2GENE = msigdb_go_pathways,
                     TERM2NAME = NA, verbose = TRUE, seed = FALSE, by = "fgsea")
head(gsea_go_moran)

gsea_kegg_moran = GSEA(ranked_genelst, exponent = 1, nPerm = 1000, minGSSize = 10,
                       maxGSSize = 500, pvalueCutoff = 1, pAdjustMethod = "BH", TERM2GENE = msigdb_kegg_pathways,
                       TERM2NAME = NA, verbose = TRUE, seed = FALSE, by = "fgsea")
head(gsea_kegg_moran)

gsea_reactome_moran = GSEA(ranked_genelst, exponent = 1, nPerm = 1000, minGSSize = 10,
                           maxGSSize = 500, pvalueCutoff = 1, pAdjustMethod = "BH", TERM2GENE = msigdb_reactome_pathways,
                           TERM2NAME = NA, verbose = TRUE, seed = FALSE, by = "fgsea")
head(gsea_reactome_moran)

gsea_positional_moran = GSEA(ranked_genelst, exponent = 1, nPerm = 1000, minGSSize = 10,
                             maxGSSize = 500, pvalueCutoff = 1, pAdjustMethod = "BH", TERM2GENE = msigdb_positional,
                             TERM2NAME = NA, verbose = TRUE, seed = FALSE, by = "fgsea")
head(gsea_positional_moran)
