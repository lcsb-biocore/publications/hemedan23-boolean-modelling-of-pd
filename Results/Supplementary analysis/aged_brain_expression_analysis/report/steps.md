---
Title: "Boolean analysis of the  WNT/PI3K signaling"
By: "(Ahmed Abdelmonem Hemedan)"
date: "4/9/2020"
---
* Use the labels in the first column as row names
* Filter out tissue samples which are not from the midbrain / substantia nigra region
* Create quality report
* Remove all samples failing at least two quality tests
* Detect outliers via hierarchical clustering and PCoA
* Data transformation using Variance stabilising normalization (VSN)
1. Check for intensity-dependent variance
2. Verify the fit
3. Power calculation
* DEG Analysis of individual datasets
1. compute simple linear model fit to microarray data
2. extract the ranking table and show the top-ranked genes
* Check identity of rownames between the two datasets
* Convert Gene IDs to Gene Symbols in order to be able for further pathway analyses(important to design simulation experiments)
1. map probes to microarray rownames
2. extract gene symbols corresponding to microarray Probe IDs (take always the first symbol mapped)
3. get annotations to convert Affymetrix gene IDs to official HGNC gene symbols
4. extract the gene symbol information
5. get the probe identifiers that are mapped to a gene name
6. extract the gene description information
* Convert expression matrix with Affymetrix IDs to Gene Symbol matrix (multiple probes match to a gene, take the max. average value probe as representative for the gene)
* Differential expression analysis at the gene level (instead of probe level)
* Pathway analyses
1.  Load pathway definitions from MSigDB
2.  Apply classical Fisher's Exact test (significance-of-overlap computation)
3.  Apply GSEA
* perform binarization with k-means
* select the diffrentially expressed genes from the whole data
* Creates a BooleanNetwork object with exactly one function per gene by extracting a specified set of transition functions
* Getting Attractors
* Save network ad boolean dpendencies ans SBML-Qual
